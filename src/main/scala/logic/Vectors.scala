package logic
import simulation._
import simulation.config.systemConfig

case class Vector3D(x: Double, y: Double, z: Double):

    def +(other: Vector3D): Vector3D = Vector3D(x + other.x, y + other.y, z + other.z)

    def -(other: Vector3D): Vector3D = Vector3D(x - other.x, y - other.y, z - other.z)

    def *(d: Double): Vector3D = Vector3D(x * d, y * d, z * d)

    def dot(other: Vector3D): Double = x * other.x + y * other.y + z * other.z

    def / (d: Double): Vector3D = Vector3D(x / d, y / d, z / d)

    def length: Double = math.sqrt(this dot this)

    def unit: Vector3D = this / length

    def distanceTo(other: Vector3D) = math.sqrt((this - other) dot (this - other))
    
    def to2D = new Vector2D(x, y)

    def scaled2D(center: Vector3D): Vector2D =
        new Vector2D((x - center.x) / systemConfig.scale + config.offsetX, (y - center.y) / systemConfig.scale + config.offsetY)

class Vector2D(x: Double, y: Double) extends Vector3D(x, y, 0):

    def +(other: Vector2D): Vector2D = new Vector2D(x + other.x, y + other.y)

    def -(other: Vector2D): Vector2D = new Vector2D(x - other.x, y - other.y)

    override def *(d: Double): Vector2D = new Vector2D(x * d, y * d)

    override def / (d: Double): Vector2D = new Vector2D(x / d, y / d)

    override def unit: Vector2D = this / length

    def k: Double = y / x

    def angleTo(other: Vector2D): Double = Math.acos((this dot other) / (this.length * other.length))

    // In Degrees
    def direction: Double =
        val a = Math.atan(this.y/this.x) * 180 / Math.PI
        var fullAngle = if x == 0 && y > 0 then
            90
        else if x == 0 && y < 0 then
            270
        else if y == 0 && x > 0 then
            0
        else if y == 0 &&  x < 0 then
            180
        else if x > 0 && y > 0 then
            a
        else if x < 0 then
            a + 180
        else
            a + 360

        360 - fullAngle


object Vector3D:
    /**
     * Vector from pos1 to pos2
     */
    def apply(pos1: Vector3D, pos2: Vector3D): Vector3D = pos2 - pos1

object Vector2D:
    /**
     * Vector from pos1 to pos2
     */
    def apply(pos1: Vector2D, pos2: Vector2D): Vector2D = pos2 - pos1

    def apply(pos1: Vector3D, pos2: Vector3D): Vector2D = new Vector2D(pos2.x, pos2.y) - new Vector2D(pos1.x, pos1.y)
