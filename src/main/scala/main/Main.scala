package main

import scalafx.animation.AnimationTimer
import scalafx.application.JFXApp3
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.layout.{Background, HBox, Pane, VBox}
import scalafx.scene.input.MouseButton
import scalafx.scene.paint.Color
import logic.*
import simulation.*
import config.{offsetX, offsetY, systemConfig, windowWidth}
import scalafx.beans.property.StringProperty
import scalafx.scene.control.*
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import scalafx.geometry.Pos
import scala.collection.mutable.Queue

object Main extends JFXApp3:
    var valid = false
    var objects: Vector[Object] = _
    var starMass: Double = _
    var system: PlanetarySystem = _

    while !valid do
        println("Available systems:")
        println(
            getListOfFiles("./system-config")
                .intersect(getListOfFiles("./planets"))
                .intersect(getListOfFiles("./natural-satellites"))
                .mkString("\n")
        )

        config.systemFileName = scala.io.StdIn.readLine("Enter file name: ")

        try
            objects = Vector()
            val parser = SystemParser()

            parser.setSystemConfig()

            // load and group objects
            objects :++= parser.getPlanets
            objects :++= parser.getNaturalSatellites(objects)

            config.maxInitialOrbit = objects.map(_.pos.length).max

            starMass = objects.map(_.mass).max
            config.starsCount = objects.sortBy(_.pos.length).count(_.mass / starMass > 0.1)

            system = PlanetarySystem(objects, systemConfig.simulationStep)
            val names = objects.map(_.name)
            if names.distinct.size == names.size then
                valid = true
            else
                println("Object names across all files must be unique")
        catch
            case e: Error =>
                e.printStackTrace()
                println("Invalid file")

    var trajectorySystems: Vector[TrajectorySystem] = Vector()

    def resetTrajectorySystems() =
        config.resetting = true
        var objs = system.objects

        val star = combineStars(objs.stars)
        val extras = system.extraObjects

        var newTrajectorySystems: Vector[TrajectorySystem] = Vector()
        val innerPlanets = (objs.diff(objs.satellites).diff(objs.stars) ++ Iterable(star)) // .sortBy(_.pos.length).take(config.innerPlanetCount + config.starsCount).clone
        val outerPlanets = (objs.diff(objs.satellites).sortBy(_.pos.length).drop(systemConfig.innerPlanetCount + config.starsCount) ++ extras ++ Iterable(star))
        val satellitePlanets = (objs.diff(objs.stars) ++ Iterable(star))

        // create trajectory systems
        newTrajectorySystems :+= TrajectorySystem(
            innerPlanets.clone,
            systemConfig.simulationStep * systemConfig.innerTrajectoryStepMultiplier,
            system,
            innerPlanets.dropRight(1).map(_.approxOrbitalPeriod).sorted.take(systemConfig.innerPlanetCount + config.starsCount).max * 3
        )

        newTrajectorySystems :+= TrajectorySystem(
            outerPlanets.clone,
            systemConfig.simulationStep * systemConfig.outerTrajectoryStepMultiplier,
            system,
            outerPlanets.dropRight(1).map(_.approxOrbitalPeriod).max * 3
        )

        newTrajectorySystems :+= TrajectorySystem(
            satellitePlanets.clone,
            systemConfig.simulationStep * systemConfig.satelliteTrajectoryStepMultiplier,
            system,
            objects.satellites.map(_.approxOrbitalPeriod).max * 3
        )

        while !newTrajectorySystems.forall(_.ready) do Thread.onSpinWait()
        trajectorySystems.foreach(_.disable())
        newTrajectorySystems.foreach(ts => startThread(Thread(TrajectorySimultator(ts))))
        trajectorySystems = newTrajectorySystems

        config.resetting = false

    resetTrajectorySystems()
    while !trajectorySystems.forall(_.ready) do Thread.onSpinWait()

    var radius = objects.spherical.map(_.radius)
    val starRadius = radius.max
    radius = radius.filter(_ / starRadius < 0.5) // Only include radius smaller than half of largest to exclude stars
    val minRadius = radius.min
    val maxRadius = radius.max
    val radiusScale = (systemConfig.maxDrawRadius - systemConfig.minDrawRadius) / (maxRadius - minRadius)

    var lastMouseDown = (0.0, 0.0)
    var lastOffset = (offsetX, offsetY)

    var centerObject = system.objects.minBy(_.pos.length)

    // start simulator
    startThread(Thread(MainSimulator(system, resetTrajectorySystems)))

    def updateInfo() =
        info.simulationStep.set(s"Simulation Step: ${systemConfig.simulationStep} s")
        info.name.set("Name: " + centerObject.name)
        info.mass.setText(centerObject.mass.toString)
        info.radius.setText(
            centerObject match
                case o: SphericalObject => o.radius.toString
                case _ => ""
        )
        info.vel.setText(centerObject.velocity.length.toString)

    def start() =

        stage = new JFXApp3.PrimaryStage:
            title = "Planetary System Simulator"
            resizable = false

        val map = new Canvas(config.windowWidth, config.windowHeight):
            val gc = graphicsContext2D

            gc.fillPath()

            def drawObjects() =
                for obj <- system.allObjects do
                    obj match
//                        case o: NaturalSatellite => ???
                        case o: SphericalObject =>
                            // TODO Switch to true scale when zooming in
                            val radius = if o.radius / starRadius < 0.5 then
                                systemConfig.minDrawRadius + radiusScale * (o.radius - minRadius)
                            else
                                50
                            val pos = o.pos.scaled2D(centerObject.pos)
                            gc.fillOval(pos.x - radius/2, pos.y - radius/2, radius, radius)
                        case o: SpaceCraft =>
                            ???

            def drawTrajectories() =
                trajectorySystems.flatMap(_.relevantObjects).foreach(obj =>
                    val pp1 = obj.previousPositions.clone()

                    var closed = true
                    var orbitPoints = 0
                    var exact = false

                    // number of points for one orbit
                    val pointDistance = Vector2D(pp1.head.pos, pp1(1).pos).length
                    val approxIndex = pp1.drop(5).indexWhere(p => Vector2D(pp1.head.pos, p.pos).length < pointDistance * 4)
                    // Trajectory is not closed if no such index was found
                    if approxIndex == -1 then
                        closed = false
                    else
                        val timeOfClosest = pp1.slice(5, (approxIndex + systemConfig.trajectoryPoints/5))
                            .minBy(p => Vector2D(pp1.head.pos, p.pos).length).time
                        orbitPoints = pp1.indexWhere(_.time == timeOfClosest) + 1
                        val perpendicularDistanceOfLast = perpendicularDistanceFrom(pp1.head.pos, pp1(1).pos, pp1(orbitPoints-1).pos)
                        if perpendicularDistanceOfLast < pointDistance / 5 then exact = true

                    val pp = obj match
                        case ns: NaturalSatellite =>
                            val parentPos = system.objects.get(ns.parent.name).pos
                            ns.previousPositions.map(parentPos + _.pos)
                        case _ =>
                            obj.previousPositions.map(_.pos)

                    var points: Seq[Queue[Vector3D]] = null

                    // If the orbit is closed, exact and near circular, connect the ends. If the orbit is only closed,
                    // take enough points for one orbit. If the orbit is open, use all points.
                    if closed && exact && isNearCircular(pp1.take(orbitPoints)) then
//                        pp.take(150).foreach(p => gc.fillOval(p.scaled2D(centerObject.pos).x - 2, p.scaled2D(centerObject.pos).y - 2, 4, 4))
//                        gc.setFill(Color.Red)
//                        gc.fillOval(pp.drop(systemConfig.trajectoryPoints/15).head.scaled2D(centerObject.pos).x - 4, pp.drop(systemConfig.trajectoryPoints/15).head.scaled2D(centerObject.pos).y - 4, 8, 8)
//                        gc.setFill(Color.Blue)
//                        gc.fillOval(pp(orbitPoints - systemConfig.trajectoryPoints / 15).scaled2D(centerObject.pos).x - 4, pp(orbitPoints - systemConfig.trajectoryPoints / 15).scaled2D(centerObject.pos).y - 4, 8, 8)
//                        gc.setFill(Color.Black)

                        points = pp.slice(systemConfig.trajectoryPoints / 15, orbitPoints - systemConfig.trajectoryPoints / 15)
                            .sliding(3, 2)
                            .takeWhile(_.size == 3)
                            .toSeq

                        points = points :+ Queue(
                            points.flatten.last,
                            // Force the non perfect trajectory to go through the object
                            if info.orbitSnap.selected.get() then
                                system.allObjects.get(obj.name).pos
                            else
                                pp.head
                            ,
                            points.flatten.head
                        )
                    else if closed then
                        points = pp.take(orbitPoints)
                            .sliding(3, 2)
                            .takeWhile(_.size == 3)
                            .toSeq
                    else
                        points = pp
                            .sliding(3, 2)
                            .takeWhile(_.size == 3)
                            .toSeq

                    gc.beginPath()

                    points
                        .foreach( points =>
                            // TODO use line if points are very close to a line
                            val scaledPoints = points.map(_.scaled2D(centerObject.pos))
                            val c: Vector2D = center(scaledPoints)
                            val r = Vector2D(c, scaledPoints(1)).length
                            gc.arc(
                                c.x, c.y,
                                r, r,
                                Vector2D(c, scaledPoints.head).direction,
                                -Vector2D(c, scaledPoints.head).angleTo(Vector2D(c, scaledPoints.last)) * 180 / Math.PI
                            )
                        )
                    gc.strokePath()
                    gc.closePath()
                )

            def reDraw() =
                gc.clearRect(0, 0, width.value, height.value)

                drawObjects()
                var trjectoriesDone = false
                while !trjectoriesDone do
                    try
                        drawTrajectories()
                        trjectoriesDone = true
                    catch
                        case e: java.lang.IndexOutOfBoundsException => ()

            val drawTimer = AnimationTimer.apply(_ => reDraw())
            drawTimer.start()

            onScroll = (event) =>
                systemConfig.scale *= Math.pow(1.05, -event.getDeltaY / Math.abs(event.getDeltaY))
                if systemConfig.scale < systemConfig.minScale then systemConfig.scale = systemConfig.minScale

            onMousePressed = (event) =>
                lastMouseDown = (event.getX, event.getY)
                lastOffset = (offsetX, offsetY)

            onMouseDragged = (event) =>
                offsetX = lastOffset._1 - lastMouseDown._1 + event.getX
                offsetY = lastOffset._2 - lastMouseDown._2 + event.getY

            onMouseClicked = (event) =>
                if
                    event.getButton == MouseButton.Primary.delegate
                    && event.getX == lastMouseDown._1
                    && event.getY == lastMouseDown._2
                then
                    val x = (event.getX - offsetX) * systemConfig.scale + centerObject.pos.x
                    val y = (event.getY - offsetY) * systemConfig.scale + centerObject.pos.y

                    if config.changingDirection then
                        centerObject.velocity = Vector3D(centerObject.pos, Vector3D(x, y, centerObject.pos.z)).unit * centerObject.velocity.length
                        config.changingDirection = false
                    else if config.moving then
                        centerObject.pos = Vector3D(x, y, centerObject.pos.z)
                        config.moving = false
                    else if config.adding.state == 1 then
                        config.adding.pos = Vector3D(x, y, 0)
                        config.adding.state += 1
                    else if config.adding.state == 2 then
                        val vel = Vector3D(config.adding.pos, Vector3D(x, y, 0)).unit
                        centerObject = SphericalObject(java.util.UUID.randomUUID.toString, 1, config.adding.pos, vel, 1)
                        system.addExtra(centerObject)
                        config.adding.state = 0
                    else
                        val newCenter = system.allObjects.minBy(o => Math.hypot(x - o.pos.x, y - o.pos.y))
                        if newCenter != centerObject then
                            offsetX = config.windowWidth  / 2.0
                            offsetY = config.windowHeight / 2.0
                        centerObject = newCenter
                    updateInfo()

        val infoBox = new VBox():
            def changeTPS(value: String) =
                value.toIntOption match
                    case Some(value) =>
                        config.simulationRate = value
                        Counter.reset()
                    case None => ()

            def changeMass(value: String) =
                value.toDoubleOption match
                    case Some(value) => if value != 0 then centerObject.mass = value
                    case None => ()

            def changeRadius(value: String) =
                centerObject match
                    case o: SphericalObject =>
                        value.toDoubleOption match
                            case Some(value) => if value != 0 then o.radius = value
                            case None => ()
                    case _ => ()

            def changeVelocity(value: String) =
                value.toDoubleOption match
                    case Some(value) => if value != 0 then centerObject.velocity = centerObject.velocity.unit * value
                    case None => ()

            val targetTPS = ValueBox("Target Tick Rate: ", "", changeTPS, "\\D")
            targetTPS.tf.setText(config.simulationRate.toString)
            val tps = new Label()
            val step = new Label()
            val orbitSnap = CheckBox("Snap Orbits to Objects")
            orbitSnap.setSelected(true)
            info.orbitSnap = orbitSnap

            val name = new Label()
            val mass = ValueBox("Mass: ", "kg", changeMass, "[^\\d\\.E]")
            val radius = ValueBox("Radius: ", "m", changeRadius, "[^\\d\\.E]")
            val vel = ValueBox("Velocity: ", "m/s", changeVelocity, "[^\\d\\.E]")
            val direction = Button("Change Direction")
            direction.setOnAction( (event) => config.changingDirection = true )
            val move = Button("Move Object")
            move.setOnAction( (event) => config.moving = true )

            val create = Button("Create New Object")
            create.setOnAction( (event) =>
                config.simulationRate = 0
                Counter.reset()
                config.adding.state = 1
            )

            val warning = Label("")
            warning.setBackground(Background.fill(Color.Red))

            info.mass = mass.tf
            info.radius = radius.tf
            info.vel = vel.tf

            tps.textProperty().bind(info.tps)
            step.textProperty().bind(info.simulationStep)
            name.textProperty().bind(info.name)
            warning.textProperty().bind(info.warning)

            background = Background.fill(Color.Gray)
            children = Array(
                targetTPS, tps, step, orbitSnap, new Separator,
                name, mass, radius, vel, direction, move, new Separator,
                create, new Separator,
                warning
            )

        val root = new Pane():
            children = Array(map, infoBox)

        val scene = new Scene(parent = root)
        stage.scene = scene
        updateInfo()


class ValueBox(label: String, unit: String, onAction: String => Unit, replace: String) extends HBox():
    alignment = Pos.CenterLeft
    val l = new Label(label)
    val value = new TextField()
    // https://stackoverflow.com/a/30796829 (muutettu scalaksi javasta)
    value.textProperty.addListener(new ChangeListener[String]() {
        override def changed(observable: ObservableValue[? <: String], oldValue: String, newValue: String): Unit = {
            value.setText(newValue.replaceAll(replace, ""))
        }
    })
    val u = new Label(unit)
    val set = Button("Set")
    set.setOnAction( (event) => onAction(value.getText) )
    children = Array(l, value, set)

    def tf = value
