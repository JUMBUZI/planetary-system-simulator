package logic

import scala.collection.mutable.Queue

import simulation._
import simulation.config.systemConfig
import main.combineStars

val GRAVITATIONAL_CONSTANT = 0.000000000066743
val SOL_MASS = 1988500000000000000000000000000.0
val AU = 149597870700.0
val SEC_IN_YR = 31556925.9747

extension (v: Vector[Object])
    def clone: Vector[Object] =
        val satellites: Vector[NaturalSatellite] = v.satellites
        val nonSatellites = v.diff(satellites)

        val nonSatelliteCopies = nonSatellites.map(_.copy)
        val satelliteCopies = satellites.map(_.copy(nonSatelliteCopies))

        nonSatelliteCopies ++ satelliteCopies

    // TODO error handling
    def get(name: String): Object = v.find(_.name == name).get

    def spherical: Vector[SphericalObject] = v.collect { case o: SphericalObject => o }

    def satellites: Vector[NaturalSatellite] = v.collect { case s: NaturalSatellite => s }

    def stars: Vector[SphericalObject] = v.sortBy(_.pos.length).take(config.starsCount).collect { case o: SphericalObject => o }


extension (t: Tuple3[Double, Vector3D, Vector3D])
    def time = t._1
    def pos = t._2
    def vel = t._3


// mass in kg
sealed trait Object(val name: String, var mass: Double, var pos: Vector3D, var velocity: Vector3D):

    protected val prevPos: Queue[(Double, Vector3D, Vector3D)] = Queue()
    private var dt: Double = _
    var system: PlanetarySystem = _

    def simulateTick(objects: Vector[Object], objectForces: Map[Set[Object], Double]): Unit =
        val totalForce =
            objects
                .filterNot(_ == this)
                .map( obj => Vector3D(pos, obj.pos).unit * objectForces(Set(this, obj)) )
                .reduce(_ + _)

        // Semi-implicit Euler
        savePrevPos()
        velocity += acceleration(totalForce) * dt
        pos += velocity * dt

    def getPos = pos

    def getVel = velocity

    def getMass = mass

    // force in N
    def forceBetween(other: Object): Double =
        (GRAVITATIONAL_CONSTANT * mass * other.mass / math.pow(pos distanceTo other.pos, 2))

    def acceleration(force: Vector3D): Vector3D = force / mass

    def setSimulationStep(newStep: Double) =
        dt = newStep

        if prevPos.isEmpty then
            system match
                case ts: TrajectorySystem => prevPos.enqueue((ts.time-dt, pos - velocity*dt, velocity))
                case _ => ()

    private def savePrevPos(): Unit =
        system match
            case ts: TrajectorySystem =>
                if ts.time >= prevPos.last.time + saveInterval then
                    prevPos.enqueue((ts.time, posToSave(pos), velToSave(velocity)))
                // delete older than 1 max orbit
                while prevPos.length > 1 && ts.time - prevPos.head.time > ts.ahead do
                    prevPos.dequeue()
            case _ => ()

    protected def posToSave(v: Vector3D): Vector3D = v

    protected def velToSave(v: Vector3D): Vector3D = v

    def previousPositions = prevPos.clone()

    def copy: Object

    lazy val approxOrbitalPeriod: Double

    lazy val saveInterval: Double = approxOrbitalPeriod / systemConfig.trajectoryPoints


class SphericalObject(name: String, mass: Double, pos: Vector3D, velocity: Vector3D, var radius: Double)
    extends Object(name, mass, pos, velocity):

    def copy: SphericalObject =
        SphericalObject(this.name, getMass, getPos, getVel, radius)

    lazy val approxOrbitalPeriod: Double =
        Math.min(Math.sqrt(Math.pow(getPos.length / AU, 3) / (combineStars(system.objects.stars).mass / SOL_MASS)) * SEC_IN_YR, 1000 * SEC_IN_YR)

    def getRadius = radius


class NaturalSatellite(name: String, val parent: Object, mass: Double, pos: Vector3D, velocity: Vector3D, radius: Double)
    extends SphericalObject(name, mass, pos, velocity, radius):

    def copy(newParents: Vector[Object]): NaturalSatellite =
        new NaturalSatellite(this.name, newParents.get(parent.name), getMass, getPos, getVel, getRadius)

    override lazy val approxOrbitalPeriod: Double =
        val starMass = parent.mass / SOL_MASS
        Math.min(Math.sqrt(Math.pow(Vector3D(getPos, parent.pos).length / AU, 3) / starMass) * SEC_IN_YR, 1000 * SEC_IN_YR)

    override def posToSave(v: Vector3D): Vector3D = Vector3D(parent.pos, v)

    override def velToSave(v: Vector3D): Vector3D = v - parent.velocity


class SpaceCraft(name: String, mass: Double, pos: Vector3D, velocity: Vector3D, val fuel: Double, val maxThrust: Double, val maxFuelConsuption: Double)
    extends Object(name, mass, pos, velocity):

    def copy: SpaceCraft = new SpaceCraft(this.name, getMass, getPos, getVel, this.fuel, this.maxThrust, this.maxFuelConsuption)

    lazy val approxOrbitalPeriod: Double = ???
