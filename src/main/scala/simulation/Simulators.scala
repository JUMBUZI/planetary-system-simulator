package simulation

import logic.*
import scalafx.application.Platform

import scala.concurrent.Future
import concurrent.ExecutionContext.Implicits.global

class MainSimulator(system: PlanetarySystem, reset: () => Unit) extends Runnable:
    def run() =
        val shortestOrbit = system.objects.diff(system.objects.satellites)
            .sortBy(_.pos.length)
            .drop(config.starsCount)
            .map(_.approxOrbitalPeriod)
            .min

        while true do
            if System.nanoTime >= Counter.expectedTickTime then
                system.simulateTick()
                Counter.tick()
                if Counter.tickCount % 10_000 == 0 then
                    Platform.runLater(new Runnable() {
                        override def run(): Unit = {
                            info.tps.set(s"Tick Rate: ${Counter.tps.toInt} ticks/s")
                            if system.allObjects.map(_.pos.length).max > config.maxInitialOrbit * 5 then
                                info.warning.set("Warning: One ore move objects are far away from the center")
                            else
                                info.warning.set("")
                        }
                    })

                if !config.resetting then Future(reset())
            else
                Thread.onSpinWait()

class TrajectorySimultator(val system: TrajectorySystem) extends Runnable:
    def run() =
        while system.inUse do
            if system.time < system.main.time then
                system.simulateTick()
            else
                Thread.onSpinWait()
