package simulation

import logic.Vector3D
import scalafx.beans.property.StringProperty
import scalafx.scene.control.{CheckBox, TextField}

object config:
    var changingDirection = false
    var moving = false
    object adding:
        var state: Int = 0
        var pos: Vector3D = _
    var resetting = false

    var starsCount: Int = 0

    var simulationRate: Int = 10000 // Ticks per second

    val windowWidth: Int = 1000
    val windowHeight: Int = 1000

    var maxInitialOrbit: Double = _

    var offsetX: Double = windowWidth / 2.0
    var offsetY: Double = windowHeight / 2.0

    var systemFileName: String = "2460408.500000000-TDB.json"

    var systemConfig: SystemConfig = _


class SystemConfig(
                      var innerPlanetCount: Int,

                      // meters per pixel
                      var scale: Double,
                      var minScale: Double,

                      // Objects will get a dislplayed radius between these values based on their actual radius
                      val minDrawRadius: Double,
                      val maxDrawRadius: Double,

                      var simulationStep: Double, // Seconds, Luna breaks at some point between 60-120
                      var satelliteTrajectoryStepMultiplier: Int,
                      var innerTrajectoryStepMultiplier: Int,
                      var outerTrajectoryStepMultiplier: Int,

                      // per orbit, approximatem should be at least 30, 100-200 seems optimal
                      var trajectoryPoints: Int
                  )

object info:
    val tps = StringProperty("")
    val simulationStep = StringProperty("")
    val warning = StringProperty("")
    var orbitSnap: CheckBox = _

    val name = StringProperty("")
    var mass: TextField = _
    var radius: TextField = _
    var vel: TextField = _
