package main

import java.io.File
import scala.collection.mutable.Queue
import scala.collection.mutable.ListBuffer
import logic._

def center(points: Queue[Vector2D]): Vector2D =
    require(points.length == 3)
    val p1 = points(0)
    val p2 = points(1)
    val p3 = points(2)

    val p12half = p1 + (Vector2D(p1, p2) / 2)
    val p13half = p1 + (Vector2D(p1, p3) / 2)

    val p12perpedicularSlope = -1 / Vector2D(p1, p2).k
    val p13perpedicularSlope = -1 / Vector2D(p1, p3).k

    val x = (p12perpedicularSlope*p12half.x - p13perpedicularSlope*p13half.x - p12half.y + p13half.y) / (p12perpedicularSlope - p13perpedicularSlope)
    val y = p12perpedicularSlope * (x - p12half.x) + p12half.y

    new Vector2D(x, y)

// Ignores z
def perpendicularDistanceFrom(p1: Vector3D, p2: Vector3D, point: Vector3D) =
    val x0 = p1.x
    val y0 = p1.y
    val k = Vector2D(p1, p2).k

    val a = -k
    val b = 1
    val c = k * x0 - y0

    Math.abs(a*point.x + b*point.y + c) / Math.sqrt(a*a + b*b)

// Ignores z
def isNearCircular(points: Queue[(Double, Vector3D, Vector3D)]): Boolean =
    val angles: ListBuffer[Double] = ListBuffer()
    angles += Math.abs(points.head.pos.to2D.angleTo(points.head.vel.to2D) - Math.PI / 2.0)
    angles += Math.abs(points(points.size/4).pos.to2D.angleTo(points(points.size/4).vel.to2D) - Math.PI / 2.0)
    angles += Math.abs(points(points.size/2).pos.to2D.angleTo(points(points.size/2).vel.to2D) - Math.PI / 2.0)
    angles += Math.abs(points(points.size/4*3).pos.to2D.angleTo(points(points.size/4*3).vel.to2D) - Math.PI / 2.0)

    angles.forall(_ < 0.3)

def startThread(thread: Thread) =
    thread.setDaemon(true)
    thread.start()

// https://alvinalexander.com/scala/how-to-list-files-in-directory-filter-names-scala/
def getListOfFiles(dir: String):List[String] =
    val d = new File(dir)
    if (d.exists && d.isDirectory) then
        d.listFiles.filter(_.isFile).map(_.getName).toList
    else
        List[String]()

def combineStars(stars: Vector[SphericalObject]) =
    SphericalObject(
        stars.map(_.name).mkString,
        stars.map(_.mass).sum,
        stars.map(_.pos).reduce(_ + _) / stars.length,
        stars.map(_.velocity).reduce(_ + _) / stars.length,
        stars.map(_.radius).sum / stars.length,
    )