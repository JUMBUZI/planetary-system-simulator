package simulation

import simulation.config

object Counter:
    private var start = System.nanoTime
    private var t1 = start
    private var t2 = start

    var tickCount = 0L

    def tick() =
        tickCount += 1
        if tickCount % 10_000 == 0 then
            t1 = t2
            t2 = System.nanoTime()

    def reset() =
        start = System.nanoTime
        tickCount = 0L

    // Ticks per second
    def tps = 1 / ((t2 - t1) / 10_000.0 / 1_000_000_000.0)

    def expectedTickTime =
        if config.simulationRate == 0 then
            Long.MaxValue
        else
            start + tickCount * 1_000_000_000 / config.simulationRate