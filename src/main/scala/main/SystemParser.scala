package main

import cats.syntax.either._
import io.circe._

import logic._
import simulation._


/**
 * Reference frame: ICRF
 * Coordinate Center: Solar System Barycenter
 *
 * mass: kg
 * pos/radius: m
 * velocity: m/s
 */
class SystemParser:

    private implicit val decodeSysConf: Decoder[SystemConfig] = (c: HCursor) => for {
        innerPlanetCount <- c.downField("innerPlanetCount").as[Int]
        scale <- c.downField("scale").as[Double]
        minScale <- c.downField("minScale").as[Double]
        minDrawRadius <- c.downField("minDrawRadius").as[Double]
        maxDrawRadius <- c.downField("maxDrawRadius").as[Double]
        simulationStep <- c.downField("simulationStep").as[Double]
        satelliteTrajectoryStepMultiplier <- c.downField("satelliteTrajectoryStepMultiplier").as[Int]
        innerTrajectoryStepMultiplier <- c.downField("innerTrajectoryStepMultiplier").as[Int]
        outerTrajectoryStepMultiplier <- c.downField("outerTrajectoryStepMultiplier").as[Int]
        trajectoryPoints <- c.downField("trajectoryPoints").as[Int]
    } yield {
        new SystemConfig(
            innerPlanetCount, scale, minScale, minDrawRadius, maxDrawRadius,
            simulationStep, satelliteTrajectoryStepMultiplier, innerTrajectoryStepMultiplier,
            outerTrajectoryStepMultiplier, trajectoryPoints
        )
    }

    private implicit val decodeVector3D: Decoder[Vector3D] = (c: HCursor) => for {
        x <- c.downN(0).as[Double]
        y <- c.downN(1).as[Double]
        z <- c.downN(2).as[Double]
    } yield {
        new Vector3D(x, y, z)
    }

    private implicit val decodeSphericalObject: Decoder[SphericalObject] = (c: HCursor) => for {
        name <- c.downField("name").as[String]
        mass <- c.downField("mass").as[Double]
        pos <- c.downField("position").as[Vector3D]
        vel <- c.downField("velocity").as[Vector3D]
        radius <- c.downField("radius").as[Double]
    } yield {
        new SphericalObject(name, mass, pos, vel, radius)
    }

    private def readFile(path: String): String =
        val source = scala.io.Source.fromFile(path)
        try
            source.mkString
        finally
            source.close()

    def setSystemConfig() =
        val jsonString = readFile("system-config/" + config.systemFileName)

        val json = parser.parse(jsonString)
        val sysConf = json
            .leftMap(err => err: Error)
            .flatMap(_.as[SystemConfig])

        sysConf match
            case Left(error) => throw error
            case Right(c) => config.systemConfig = c

    def getPlanets: Vector[Object] =
        val jsonString = readFile("planets/" + config.systemFileName)

        val json = parser.parse(jsonString)
        val objects = json
            .leftMap(err => err: Error)
            .flatMap(_.as[Vector[SphericalObject]])

        objects match
            case Left(error) => throw error
            case Right(objectList) => objectList

    def getNaturalSatellites(parents: Vector[Object]): Vector[Object] =
        implicit val decodeNaturalSatellite: Decoder[NaturalSatellite] = (c: HCursor) => for {
            name <- c.downField("name").as[String]
            parent <- c.downField("parent").as[String]
            mass <- c.downField("mass").as[Double]
            pos <- c.downField("position").as[Vector3D]
            vel <- c.downField("velocity").as[Vector3D]
            radius <- c.downField("radius").as[Double]
        } yield {
            new NaturalSatellite(name, parents.get(parent), mass, pos, vel, radius)
        }
        
        val jsonString = readFile("natural-satellites/" + config.systemFileName)

        val json = parser.parse(jsonString)
        val objects = json
            .leftMap(err => err: Error)
            .flatMap(_.as[Vector[NaturalSatellite]])

        objects match
            case Left(error) => throw error
            case Right(objectList) => objectList
