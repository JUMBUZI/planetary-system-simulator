package logic

import scala.concurrent.Future
import concurrent.ExecutionContext.Implicits.global

import simulation._
import simulation.config.systemConfig

class PlanetarySystem(val objects: Vector[Object], var simulationStep: Double):

    var extraObjects: Vector[Object] = Vector()

    protected var currentTime: Double = 0

    objects.foreach(_.system = this)
    objects.foreach(_.setSimulationStep(simulationStep))
    
    // The force between every pair of objects
    def objectForces: Map[Set[Object], Double] =
        (
            for
                (object1, i) <- allObjects.zipWithIndex
                object2 <- allObjects.drop(i + 1)
            yield
                Set(object1, object2) -> object1.forceBetween(object2)
        ).toMap

    def simulateTick() =
        val objForces = objectForces
        allObjects.foreach(_.simulateTick(allObjects, objForces))

        currentTime += simulationStep

    def time = currentTime

    def setSimulationStep(newStep: Double) =
        simulationStep = newStep
        objects.foreach(_.setSimulationStep(simulationStep))

    def addExtra(o: Object) =
        o.setSimulationStep(simulationStep)
        o.system = this
        extraObjects :+= o

    def allObjects = objects ++ extraObjects
    
class TrajectorySystem(objects: Vector[Object], simulationStep: Double, val main: PlanetarySystem, val ahead: Double) 
    extends PlanetarySystem(objects, simulationStep):

    currentTime = main.time
    
    var ready = false
    var inUse = true

    private def simulateAhead() =
        while time < main.time do
            super.simulateTick()
        ready = true

    Future(simulateAhead())

    def relevantObjects =
        if this.objects.satellites.nonEmpty then
            this.objects.satellites
        else if simulationStep == systemConfig.simulationStep * systemConfig.innerTrajectoryStepMultiplier then
            this.objects.sortBy(_.pos.length).tail.take(systemConfig.innerPlanetCount)
        else
            this.objects.sortBy(_.pos.length).tail

    def disable() = inUse = false

    override def time = super.time - ahead
