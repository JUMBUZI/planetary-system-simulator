# Planetary System Simulator
A simple program for simulating planetary systems.
![ui.png](README-images/ui.png)

# Instructions for use

The program is started from the Main object in the Main.scala file. On startup, the program lists the available files and asks the user to select a file until it receives a response that it can load. A graphical user interface is then opened. The view can be moved by dragging the mouse and zoomed by scrolling. Clicking on the window selects the nearest object. The program will update the trajectories as often as possible. Increase the TrajectoryStepMultiplier to speed up the update. In the top left corner are the controls. Explanations of the controls which are not completely obvious can be found below.

### Target Tick Rate

How many times per second the program tries to advance the simulation by Simulation Step. Choose a number well below the maximum your computer is capable of to produce a smooth simulation. If you choose too high a number, the trajectories will not be able to update. Choose a high number to simulate as fast as your computer can (Not recommended due to the previously mentioned limitation. If you do this set all TrajectoryStepMultipliers > 1). Select 0 to stop the simulation.

### Snap Orbits to Objects

Since trajectories are simulated with larger Simulation Step, they are not perfectly accurate. This setting attempts to force the trajectory to be drawn through an object. This works in most situations, and the program should turn this off automatically for individual objects when it would not work correctly, but turn it off if the orbits are not drawn correctly.

### Change Direction

The selected object will move in the direction indicated by the next mouse click at the same speed.

### Move Object

The selected object will move to the location indicated by the next mouse click.

### Create New Object

Create a new object. First the button stops the simulation. The next mouse click determines the position of the object and the next one determines the direction. The remaining attributes can be defined with other tools. To turn the simulation back on, set a new Target Tick Rate.

# Files

The program stores the data it encounters in JSON files. There are three files required for each system and they are stored in locations:
- ./planets/$SYSTEM_NAME.json
- ./natural-satellites/$SYSTEM_NAME.json
- ./system-config/$SYSTEM_NAME.json

All three files are required for the program to identify the system. Objects are stored in the first two and in the last one other settings needed by the system. The units used are standard SI units, except for mass, which is in kilograms. All data in this repository is from the NASA's Horizons system. Reference frame used by the program is ICRF and the coordinate system origin is Solar System Barycenter.

## Requirements for settings

- Object names must be unique, and parent objects of natural satellites must exist.
- All the numbers in the object files as well as scale and minScale are parsed as Double and the rest as Int.
- trajectoryPoints should be kept between 100 and 200. Values below 30 can cause strange trajectory plotting.
- If you are not using a Simulation Rate that is well below the maximum your machine is capable of, set all TrajectoryStepMultipliers > 1.
- SimulationStep > 60 will result in an unstable trajectory of the moon. > 120 will cause the moon to be ejected from its trajectory rather quickly.
- If you want to study objects far from the star, make sure that outerTrajectoryStepMultiplier is large enough. After all, the program simulates the trajectory of distant planets for a maximum of 3000 years, so even with lower numbers you don't have to wait indefinitely, but it can still take a while.